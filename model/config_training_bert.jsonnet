{
    "dataset_reader": {
        "type": "conll2003",
        "coding_scheme": "BIOUL",
        "tag_label": "ner",
        "token_indexers": {
            "bert": {
                "type": "bert-pretrained",
                "do_lowercase": false,
                "pretrained_model": "bert-base-multilingual-cased",
                "use_starting_offsets": true
            },
            "token_characters": {
                "type": "characters",
                "min_padding_length": 3
            }
        }
    },
    "iterator": {
        "type": "basic",
        "batch_size": 2
    },
    "model": {
        "type": "simple_tagger",
        "calculate_span_f1": true,
        "label_encoding": "BIOUL",
        "encoder": {
            "type": "lstm",
            "bidirectional": true,
            "dropout": 0.5,
            "hidden_size": 100,
            "input_size": 896,
            "num_layers": 2
        },
        "text_field_embedder": {
            "allow_unmatched_keys": true,
            "embedder_to_indexer_map": {
                "bert": [
                    "bert",
                    "bert-offsets"
                ],
                "token_characters": [
                    "token_characters"
                ]
            },
            "token_embedders": {
                "bert": {
                    "type": "bert-pretrained",
                    "pretrained_model": "bert-base-multilingual-cased"
                },
                "token_characters": {
                    "type": "character_encoding",
                    "embedding": {
                        "embedding_dim": 16
                    },
                    "encoder": {
                        "type": "cnn",
                        "conv_layer_activation": "relu",
                        "embedding_dim": 16,
                        "ngram_filter_sizes": [
                            3
                        ],
                        "num_filters": 128
                    }
                }
            }
        }
    },
    "train_data_path": "../data/eng.sdrt.stac/eng.sdrt.stac_train.ner.conllu",
    "validation_data_path": "../data/eng.sdrt.stac/eng.sdrt.stac_dev.ner.conllu",
    "trainer": {
        "cuda_device": -1,
        "grad_norm": 5,
        "num_epochs": 4,
        "num_serialized_models_to_keep": 1,
        "optimizer": {
            "type": "bert_adam",
            "lr": 0.001
        },
        "validation_metric": "+f1-measure-overall"
    }
}