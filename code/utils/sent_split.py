import spacy
import stanza
import numpy as np
from tqdm import tqdm
from stanza.utils.conll import CoNLL


def spliter_stanza(f_in, fi_out, lang, treebank=None):
    # get language model
    if treebank is not None:
        stanza.download(lang, package=treebank)
    else:
        stanza.download(lang) 
    processors = 'tokenize'
    nlp = stanza.Pipeline(lang, processors=processors, use_gpu=True)
    # for each doc, get the list of tokens and labels
    tok_tok_lbls = [(doc_id, doc_toks, doc_lbls) for doc_id, doc_toks, doc_lbls in tok_tokens_labels(f_in)]
    # for each doc, get the character offset of tokens
    with open(f_in, encoding='utf-8') as f_tok:
        tok_str = f_tok.read()
    tok_tok_begs = [(doc_id, doc_chars, tok_begs) for doc_id, doc_chars, tok_begs, _ in begin_toks_sents(tok_str)]
    with open(fi_out, mode='w', encoding='utf-8') as f_out:
        # parse each doc in turn
        for (doc_id, doc_toks, doc_lbls), (_, doc_chars, tok_begs) in tqdm(zip(tok_tok_lbls, tok_tok_begs), total=min(len(tok_tok_lbls), len(tok_tok_begs))):
            doc_text = rebuild_text(doc_toks, lang=lang)
            # print(doc_text)
            ann = nlp(doc_text)
            conll_str = CoNLL.conll_as_string(CoNLL.convert_dict(ann.to_dict()))
            conll_tok_begs = list(begin_toks_sents(conll_str, True))
            # we parse one doc at a time
            assert len(conll_tok_begs) == 1
            _, p_doc_chars, p_tok_begs, p_sent_begs = conll_tok_begs[0]
            try:
                assert p_doc_chars == doc_chars
            except AssertionError:
                for i, (pdc, dc) in enumerate(zip(p_doc_chars, doc_chars)):
                    if pdc != dc:
                        print(f_in, i, p_doc_chars[i - 10:i + 10], doc_chars[i - 10:i + 10])
                        raise
            # for each beginning of sentence (in the parser output), find the corresponding token index in the original .tok
            sent_beg_idc = np.searchsorted(tok_begs, p_sent_begs, side='left')
            sent_beg_idc = set(sent_beg_idc)
            # output CONLL-U file
            f_out.write('# newdoc id = ' + doc_id + '\n')
            #print('# newdoc id = ' + doc_id, file=f_out)
            tok_sent_idx = 1
            for tok_doc_idx, (tok, lbl) in enumerate(zip(doc_toks, doc_lbls), start=0):
                if tok_doc_idx in sent_beg_idc:
                    if tok_doc_idx > 0:
                        # add an empty line after the previous sentence (not for the first token in doc)
                        f_out.write('\n')
                        #print('', file=f_out)
                    tok_sent_idx = 1
                else:
                    tok_sent_idx += 1
                row = (str(tok_sent_idx), tok, '_', '_', '_', '_', '_', '_', '_', lbl)
                f_out.write('\t'.join(row)+'\n')
                #print('\t'.join(row).encode('utf-8'), file=f_out)
            f_out.write('\n')
            #print('', file=f_out)




def spliter_spacy(f_in, f_out, lg):
    lm = f"{lg}_core_web_sm"
    nlp = spacy.load(lm)
    #nlp = spacy.load("en_core_web_sm")

"""    doc = nlp(text)
    for sent in doc.sents:
        print(sent)
 """

def tok_tokens_labels(tok_filename):
    """Retrieve the list of tokens and (target) labels for each doc in a .ttok file.

    Parameters
    ----------
    tok_filename : str
        Filename of the .ttok file

    Yields
    ------
    doc_toks : List[str]
        List of tokens in the document.
    doc_lbls : List[str]
        List of labels in the document (same length as doc_toks).
    """
    with open(tok_filename, encoding='utf-8') as f:
        doc_id = None
        doc_toks = []
        doc_lbls = []
        for line in f:
            if line.startswith('# newdoc'):
                if doc_toks:
                    yield (doc_id, doc_toks, doc_lbls)
                doc_id = line.split('id = ')[1].strip()
                doc_toks = []
                doc_lbls = []
            elif line.strip() == '':
                continue
            else:
                fields = line.strip().split('\t')
                tok = fields[1]
                lbl = fields[9]
                doc_toks.append(tok)
                doc_lbls.append(lbl)
        else:
            # yield last doc
            yield (doc_id, doc_toks, doc_lbls)


def begin_toks_sents(conll_str, stanza=False):
    """Get beginning positions of tokens and sentences as offsets on the non-whitespace characters of a document text.

    Parameters
    ----------
    conll_str : str
        CONLL-U string for the file.

    Yields
    ------
    doc_id : str
        Document id.
    doc_chars : str
        Document text excluding whitespaces.
    tok_begs : List[int]
        Beginning position of each token in the doc.
        Correspond to indices in doc_chars.
    sent_begs : List[int]
        Beginning position of each sentence in the doc.
        Correspond to indices in doc_chars.
    """
    doc_id = None
    doc_chars = ''
    tok_begs = []
    sent_begs = []
    in_sent = False
    cur_idx = 0  # current (non-whitespace) character index
    for line in conll_str.split('\n'):
        if line.startswith('# newdoc id = '):
            if sent_begs:
                # yield previous doc
                yield (doc_id, doc_chars, tok_begs, sent_begs)
            # reset for a new doc
            doc_id = line.split('# newdoc id = ')[1]
            doc_chars = ''
            tok_begs = []
            sent_begs = []
            in_sent = False
            cur_idx = 0
        elif line.startswith('#'):
            continue
        elif line == '':
            # an empty line marks doc or sentence split
            in_sent = False
        else:
            fields = line.split('\t')
            assert len(fields) == 10
            if stanza and not fields[9].startswith('start'):
                continue
            # token line
            tok_begs.append(cur_idx)
            if not in_sent:
                # first token in sentence
                sent_begs.append(cur_idx)
                in_sent = True
            
            # delete whitespaces internal to the token
            tok_chars = fields[1].replace(' ', '').replace('\xa0', '')
            cur_idx += len(tok_chars)
            doc_chars += tok_chars
    else:
        # yield last document
        if sent_begs:
            yield (doc_id, doc_chars, tok_begs, sent_begs)

def rebuild_text(doc_toks, lang=None):
    """Rebuild the underlying text from a list of tokens.

    We don't assume any additional information.
    In particular, the "SpaceAfter=No" provided in some CONLL-U files is ignored.

    Parameters
    ----------
    doc_toks : List[str]
        List of tokens in the document.
    lang : str
        Language ; If None, the language is assumed to be one where tokens are
        separated with whitespaces. Currently the only interesting value is "zh"
        with no whitespace.
    """
    if lang == "zh":
        return ''.join(doc_toks)
    # default: insert whitespaces between tokens then remove extraneous ones ;
    # this heuristic is crude but a reasonable default
    doc_text = ' '.join(doc_toks)
    doc_text = (doc_text.replace(' : " ', ': "')
                .replace(' ,', ',').replace(' .', '.').replace(' !', '!').replace(' ?', '?').replace(' :', ':')
                .replace('“ ', '“').replace(' ”', '”')
                .replace(' ;', ';')
                .replace(' ’', '’')
                .replace('( ', '(').replace(' )', ')')
                .replace('[ ', '[').replace(' ]', ']')
    )
    return doc_text


def main(f_in, f_out, tool, lg):
    
    if tool == "spacy" :
        spliter_spacy(f_in,f_out, lg)
    elif tool == "stanza":
        spliter_stanza(f_in, f_out, lg, treebank=None)
    else:
        print(" pls defined sentence spliter tool : spacy, blabla")


    #return output