"""24brièvement________

1ok_______BeginSeg=Yes
2bonjour________
3tout________
4le________
5monde________
6je________
7suis________
8Ilyes________
9Rebai________
10euh________
11je________
12suis________
13un________
14ingénieur________
15de________
16recherche________
17chez________
18Linagora________

1bonjour_______BeginSeg=Yes
"""


import sys
import codecs

#input =  open(sys.argv[1],encoding="utf8").readlines()

def conll2brackets(in_f, out_f):
    start = True
    input = in_f

    with open(out_f, 'w') as file_out:
        with open(in_f, 'r') as input:
            for line in input: 
                if line.strip()=="":
                    file_out.write("]")
                    file_out.write("\n\n")
                    start = True
                else:
                    n, word, *junk, tag = line.split()
                    if tag=="BeginSeg=Yes":
                        if not(start):
                            file_out.write("] ")
                        file_out.write(f"[ {word} ")
                    else:
                        file_out.write(f"{word} ")
                    start = False
            file_out.write("]\n\n")
            
def conll2brackets_with_meta(in_f, out_f):
    start = True
    input = in_f

    with open(out_f, 'w') as file_out:
        with open(in_f, 'r') as input:
            for line in input: 
                if line.startswith("#"):
                    file_out.write(f"{line}\n")
                elif line.strip()=="":
                    file_out.write("]")
                    file_out.write("\n\n")
                    start = True
                else:
                    n, word, *junk, tag = line.split()
                    if tag=="BeginSeg=Yes":
                        if not(start):
                            file_out.write("] ")
                        file_out.write(f"[ {word} ")
                    else:
                        file_out.write(f"{word} ")
                    start = False
            file_out.write("]\n\n")