import stanza
from stanza.utils.conll import CoNLL







def with_stanza(lang, f_in, f_out, process, meta):
    """ 
    Stanza's class CoNNL:

    ID = 'id'
    TEXT = 'text'
    LEMMA = 'lemma'
    UPOS = 'upos'
    XPOS = 'xpos'
    FEATS = 'feats'
    HEAD = 'head'
    DEPREL = 'deprel'
    DEPS = 'deps'
    MISC = 'misc' -> 'start_char|end_char'
    START_CHAR = 'start_char'
    END_CHAR = 'end_char'
    FIELD_TO_IDX = {ID: 0, TEXT: 1, LEMMA: 2, UPOS: 3, XPOS: 4, FEATS: 5, HEAD: 6, DEPREL: 7, DEPS: 8, MISC: 9}
    """

    stanza.download(lang)
    nlp = stanza.Pipeline(lang, processors=process, use_gpu=True)
    with open(f_in, 'r', encoding='utf-8') as fi, open(f_out, 'w', encoding='utf-8') as fo:
        count_line = 0
        for line in fi:
            count_line += 1
            count_sent = 0
            line = line.strip()

            if line.startswith("#"):
                if "meta" in meta.keys() and meta['meta'] == True:
                    fo.write(f"{line}\n")
            elif line == "":
                fo.write("\n")
            else:
                
                #if meta['line']:
                if "line" in meta.keys():
                    txt = f"#{meta['line']}-{count_line}\n"
                    fo.write(txt)

                doc = nlp(line)
                for sent in doc.sentences:
                    count_sent += 1
                    #if meta['sent']:
                    if "sent" in meta.keys():
                        txt = f"#{meta['sent']}-{count_sent}\n#text=\"{sent.text}\"\n"
                        fo.write(txt)

                    for token in sent.tokens:
                        token_conll = CoNLL.convert_token_dict(token.to_dict()[0])
                        fo.write("\t".join(token_conll))
                        fo.write("\n")

                    fo.write("\n")

            