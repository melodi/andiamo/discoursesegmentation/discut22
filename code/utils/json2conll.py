"""
reexports allennlp predictions from json to 
conll format
"""

import json
import sys
import re

#filepath = sys.argv[1]
#config = sys.argv[2]
# conll ou tok

map = {"O":"_",
       "B-S":"BeginSeg=Yes",
       "U-S":"BeginSeg=Yes",
       "U-Conn":"Seg=B-Conn",
       "L-Conn":"Seg=I-Conn",
       "I-Conn":"Seg=I-Conn",
       "B-Conn":"Seg=B-Conn",
       "B-E":"_",
       "U-E":"_",
       }


def js2conll(filepath, fileoutpath, config):
    data = [] 
    for line in open(filepath, 'r'):
        data.append(json.loads(line))
    with open(fileoutpath, 'w') as f_out:
        for doc in data:
            tokens = zip(doc["words"],doc["tags"])
            out = "\n".join(("%s\t%s\t%s%s"%(i+1,word,"_\t"*7,map.get(tag,tag)) for (i,(word,tag)) in enumerate(tokens)))
            if config=="tok":
                print("# blabla")
            f_out.write(f'{out}\n')
            f_out.write("\n")
            #print()

def js2conllNmeta(data_pred_json, data_out, config, data_meta):
    data = []
    sent_pred_count = 0
    tok = 0
    for line in open(data_pred_json, 'r'):
        data.append(json.loads(line))

    with open(data_out, 'w', encoding='utf-8') as fo, open(data_meta, 'r') as fm:       
        
        # id 
        for line in fm:
            line = line.strip()
            if line.startswith("#"):
                fo.write(f"{line}\n")
            elif line == "":
                sent_pred_count += 1
                tok = 0
                fo.write(f"{line}\n")
            else:
                sent_pred = data[sent_pred_count]
                word = data[sent_pred_count]['words'][tok]
                tag = data[sent_pred_count]['tags'][tok]
                tok += 1
                #print(f"tok: {tok}, tag: {tag}, word: {word}, line : {line}")
                new_line = re.sub('\t[^\t]+$', '', line)
                fo.write(f"{new_line}\t{map[tag]}\n")
                #if int(line.split("\t")[0]) == tok and line.split("\t")[1] == word:
                #    fo.write(f"{line}\t{tag}\n")
                    


                #print(f"sentpred : {sent_pred}\n")
                #print(f"word n tag : {word}:::{tag}\n")
                
def js2conllNmetaNgold(data_pred_json, data_out, config, gold_n_meta):
    data = []
    sent_pred_count = 0
    tok = 0
    for line in open(data_pred_json, 'r'):
        data.append(json.loads(line))

    with open(data_out, 'w', encoding='utf-8') as fo, open(gold_n_meta, 'r') as fm:       
        
        # id 
        for line in fm:
            line = line.strip()
            if line.startswith("#"):
                fo.write(f"{line}\n")
            elif line == "":
                sent_pred_count += 1
                tok = 0
                fo.write(f"{line}\n")
            else:
                sent_pred = data[sent_pred_count]
                word = data[sent_pred_count]['words'][tok]
                tag = data[sent_pred_count]['tags'][tok]
                tok += 1
                #print(f"tok: {tok}, tag: {tag}, word: {word}, line : {line}")
                fo.write(f"{line}\t{map[tag]}\n")