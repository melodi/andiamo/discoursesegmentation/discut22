####### Python version of expes.sh

import os


def main(steps):
    dataset = steps.data.name
    config = steps.data.file # .tok .conllu
    lmodel = steps.pretr_lm #options: bert xlm elmo elmo_aligned
    action = "train" # inutile !
    evalset = steps.dev_data
    print(f"dev set : {evalset} \t trainset : {dataset}")
    has_parent = False # ?? get this var autrement. 

    tr_config = steps.tr_config 

    # cas 1 : pas de "parent", pas de "toolong"
    # cas 2 : toolong == true donc à spliter
    # cas 3 : parent == true, pas de toolong


    if lmodel == "xlm":
        bert_vocab = "xlm-roberta-base"
        bert_weights = "xlm-roberta-base"
    else : 
        bert_vocab = "bert-base-multilingual-cased"
        bert_weights = "bert-base-multilingual-cased"

    if lmodel == "bert_custom" and steps.ner_init == True :
        # TODO raise error
        print("You choose bert_custom so 'NER_format_initialisation' shall be set to false.")

    #### train, has_per == False
    # allennlp train -s Results_${CONFIG}/results_${OUTPUT} ${CODE}configs/${MODEL}.jsonnet --include-package allen_custom.custom_conll_reader --include-package allen_custom.custom_simple_tagger --include-package allen_custom.custom_disrpt_reader --include-package allen_custom.custom_bert_token_embedder 
    # allennlp train -s Resultts_conllu/results_eng.rst.rstdt_bert ../code/utils/configs/bert.jsonnet ....
    
    # Dicut- repo morteza
    #allennlp train -s Results_${CONFIG}/results_${OUTPUT} ${CODE}configs/bert.jsonnet
    cmd2 = f"allennlp train -s {steps.data.resu} {tr_config}"
    
    # Discut-gitlab
    cmd = f"allennlp train -s {steps.data.resu} {tr_config} --include-package allen_custom.custom_conll_reader --include-package allen_custom.custom_simple_tagger --include-package allen_custom.custom_disrpt_reader --include-package allen_custom.custom_bert_token_embedder" 
    
    
    
    print(cmd2)
    os.system(cmd2)
    # then...

    # TODO:
    #### train, has_par == true, en fait on fine_tune...
    #allennlp fine-tune -m Results_${CONFIG}/results_${PARENT}_${MODEL}/model.tar.gz -c ${CODE}configs/${MODEL}.jsonnet -s Results_${CONFIG}/results_${DATASET}-${PARENT}_${MODEL} --include-package allen_custom.custom_conll_reader --include-package allen_custom.custom_simple_tagger --include-package allen_custom.custom_disrpt_reader --include-package allen_custom.custom_bert_token_embedder 
    # allennlp fine-tune -m MODEL_ARCHIVE -c CONFIG_FILE -s SERIALIZATION_DIR -o overrides

    # TODO
    ### ensuite prediction sur valset ou "parent test" ou "finetune test"... ??
    #allennlp predict --use-dataset-reader --output-file Results_${CONFIG}/results_${OUTPUT}/${DATASET}_${EVAL}.predictions.json Results_${CONFIG}/results_${OUTPUT}/model.tar.gz ${TEST_A_PATH} --silent --include-package allen_custom.custom_conll_reader --include-package allen_custom.custom_simple_tagger --include-package allen_custom.custom_disrpt_reader --predictor sentence-tagger --include-package allen_custom.custom_bert_token_embedder 
