######################################
###### DISCOURSE SEGMENTOR 2022 ######
######################################
""" This the main script
    And the only one to run,
    after completion of config.json 
    Discut22 uses allennlp toolkit. For that, it need NER intermediary format.
"""

import argparse
from datetime import datetime
import os
import re
import json
import utils.syntactic_parsing as synt_pars
import utils.conv2ner as conv_to_ner # TODO clean it
import utils.json2conll as json_to_connl # TODO clean it
import utils.training_allennlp as tr_allen
import utils.conll2bracket as c2bracket
import utils.seg_eval as seg_eval
import subprocess


        

class Data:
    def __init__(self, infos, stamp, stamp_time, overwrite):
        self.name = infos['name']
        self.lang = infos['language']
        self.path = f"../data/{self.name}"
        self.exte = infos['exte']
        self.raw = f"{self.path}/{self.name}{self.exte}"
        self.stamp = stamp
        self.stamp_time = stamp_time
        self.proj = "../projects"
        self.run = f"{self.proj}/{stamp}"
        self.conv = f"{self.run}/data_converted"
        self.resu = f"{self.run}/results"
        self.over = overwrite
        self.meta = infos['existing_metadata']
        

    def create_folders(self): 
        print(f"----> Checking/creating folders.")
        if not os.path.isdir(self.proj):
            os.mkdir(self.proj)
        if not os.path.isdir(self.run):
            os.mkdir(self.run)

        if not os.path.isdir(self.conv):
            os.mkdir(self.conv)
        elif self.over == False:
            self.conv = f"{self.conv}_{stamp_time}"
            os.mkdir(self.conv)

        if not os.path.isdir(self.resu):
            os.mkdir(self.resu)
        elif self.over == False:
            self.resu = f"{self.resu}_{stamp_time}"
            os.mkdir(self.resu)

        self.resu_fine = f"{self.resu}/fine_tune"
        if os.path.isdir(self.resu_fine) and self.over == False:
            self.resu_fine = f"{self.resu_fine}_{stamp_time}"
        elif os.path.isdir(self.resu_fine) and self.over == True:
            os.rmdir(self.resu_fine)

        self.resu_train = f"{self.resu}/train"
        if os.path.isdir(self.resu_train) and self.over == False:
            self.resu_train = f"{self.resu_train}_{stamp_time}"
            

    def pre_processing(self, steps, file_in=None):
        file_in = self.raw if file_in == None else file_in
        if steps.pre_process_to_do == True:
            print(f"----> Preprocessing {self.raw}.")
            file_out = f"{self.conv}/{self.name}.conll"
            if steps.synt_tool == "stanza":
                processors = []
                metadata = {}
                if steps.toke == True:
                    processors.extend(['tokenize', 'mwt'])
                if steps.synt_parse == True:
                    processors.extend(['pos', 'lemma', 'depparse'])
                #if steps.ssplit == True:
                #    processors.append('constituency')
                if steps.crea_meta == True:
                    metadata['line'] = steps.meta_line
                    metadata['sent'] = steps.meta_sent
                if data.meta == True:
                    metadata['meta'] = True
                processors_str = ",".join(processors)
                synt_pars.with_stanza(data.lang, file_in, file_out, processors_str, metadata)
            else:
                exit(f"Exited. Not valid syntactic tool: \"{steps.synt_tool}\". Options: \"stanza\". Change your config file.")
        else:
            file_out = file_in
        logs.add_infos('data_preprocessed', file_out)
        self.preprocessed = file_out 
    
    def make_ner_format(self):
        """
        This fonction build the NER format upon the Segmentor works.
        INPUT: Tokenized text with whatever number of columns.
        OUTPUT: Tokenized text with just 4 columns.
        """
        self.ner = f"{self.preprocessed}.ner"
        self.ner = f"{self.conv}/{self.name}.ner"
        print(f"----> Making NER format {self.ner}.")
        conv_to_ner.main(self.preprocessed, self.ner, "conll") # <-- TODO faire en relatif#TODO add same for train/dev/test for config train
        logs.add_infos('data_ner', self.ner)

    def make_predictions(self, steps, js_name=None, fi_ner=None, model=None):
        js_name = self.name if js_name == None else js_name
        fi_ner = self.ner if fi_ner == None else fi_ner
        model = steps.model_path if model == None else model
        self.pred_json = f"{self.resu}/{js_name}_pred.json"
        cmd = f"allennlp predict --use-dataset-reader --output-file {self.pred_json} {model} {fi_ner} &> {self.resu}/logs_predictions.txt"
        print(f"----> Making predictions: {cmd}.")

        cp = subprocess.run(cmd, shell=True, text=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        print(f"----> done")
        with open(f"{self.resu}/logs_predictions.txt", "w") as fe:
            fe.write(cp.stderr)

        logs.add_infos('predictions_cmd', cmd)

    def pred_json_to_conll_w_metadata_w_gold(self, name=None): # here and 3 below..sorry..factorsation TBD
        name = self.name if name == None else name
        self.pred_conll_meta_gold = f"{self.resu}/{name}_pred_meta_gold.conll"
        json_to_connl.js2conllNmetaNgold(self.pred_json, self.pred_conll_meta_gold, "conll", self.preprocessed)
        return self.pred_conll_meta_gold

    def pred_json_to_conll_w_metadata(self, name=None):
        name = self.name if name == None else name
        self.pred_meta_conll = f"{self.resu}/{name}_pred_meta.conll"
        json_to_connl.js2conllNmeta(self.pred_json, self.pred_meta_conll, "conll", self.preprocessed) 
        return self.pred_meta_conll

    def pred_json_to_conll_w_gold(self, name=None):
        name = self.name if name == None else name
        self.pred_conll_gold = f"{self.resu}/{name}_pred_gold.conll"
        json_to_connl.js2conll(self.pred_json, self.pred_conll_gold, "conll") 
        return self.pred_conll_gold

    def pred_json_to_conll(self, name=None):
        name = self.name if name == None else name
        self.pred_conll = f"{self.resu}/{name}_pred.conll"
        json_to_connl.js2conll(self.pred_json, self.pred_conll, "conll") 
        return self.pred_conll

    def brackets_txt(self, name=None):
        name = self.name if name == None else name
        self.brack = f"{self.resu}/{name}_brac.txt"
        c2bracket.conll2brackets(self.pred_conll, self.brack)
        return self.brack

    def brackets_txt_with_metadata(self, name=None):
        name = self.name if name == None else name
        self.brack_meta = f"{self.resu}/{name}_brac_meta.txt"
        c2bracket.conll2brackets_with_meta(self.pred_meta_conll, self.brack_meta)
        return self.brack_meta

    def evaluation(self, steps, prod, gold=None, name=None, model=None):
        self.basic_metrics = f"{self.resu}/Evaluation_metrics.json"

        gold = self.preprocessed if gold == None else gold
        name = self.name if name == None else name
        model = steps.model_path if model == None else model

        if prod.conll_todo == False:    # get pred_file to compute metrics with seg_eval
            pred = self.pred_json_to_conll(name)
        else:                       
            if prod.conll_meta == True:
                if prod.conll_w_gold == True:
                    pred = self.pred_json_to_conll_w_metadata_w_gold(name)
                else:
                    pred = self.pred_json_to_conll_w_metadata(name)
            else:
                if prod.conll_w_gold == True:
                    pred = self.pred_json_to_conll_w_gold(name)
                else:
                    pred = self.pred_json_to_conll(name)

        print(f"----> Predictions to file {pred}.")
        print(f"----> Evaluation scores to file {self.basic_metrics}.")
        scores_dict = seg_eval.get_scores(gold, pred)
        scores_dict['model'] = model
        logs.add_infos('basic_metrics', scores_dict)
        logs.add_infos('output_conll_file', pred)

        with open(self.basic_metrics, 'w') as fo:
            json.dump(scores_dict, fo, indent=4)

        if prod.txt_todo == True:
            if prod.txt_meta == True:
                pred = f"{self.resu}/{name}_pred_meta.conll"
                if not os.path.isfile(pred):
                    self.pred_json_to_conll_w_metadata(name)
                pred_txt = self.brackets_txt_with_metadata(name)
                # os.system(f"rm {pred})
            else:
                pred = f"{self.resu}/{name}_pred.conll"
                if not os.path.isfile(pred):
                    self.pred_json_to_conll
                pred_txt = self.brackets_txt(name)
                # os.system(f"rm {pred})
            print(f"----> Predictions to file {pred_txt}.")
            logs.add_infos('output_txt_file', pred_txt)

    def make_output(self, prod):
        if prod.conll_todo == True:
            if prod.conll_meta == True:
                pred = self.pred_json_to_conll_w_metadata()
            else:
                pred = self.pred_json_to_conll()
            print(f"----> Predictions to file {pred}.")
            logs.add_infos('output_conll_file', pred)
        if prod.txt_todo == True:
            if prod.txt_meta == True:
                pred = self.pred_meta_conll = f"{self.resu}/{self.name}_pred_meta.conll"
                if not os.path.isfile(pred):
                    self.pred_json_to_conll_w_metadata()
                pred_txt = self.brackets_txt_with_metadata()
                # os.system(f"rm {pred})
            else:
                pred = self.pred_conll = f"{self.resu}/{self.name}_pred.conll"
                if not os.path.isfile(pred):
                    self.pred_json_to_conll()
                pred_txt = self.brackets_txt()
                # os.system(f"rm {pred})
            print(f"----> Predictions to file {pred_txt}.")
            logs.add_infos('output_txt_file', pred_txt)


class Output:
    def __init__(self, infos):
        self.conll_todo = infos['conll_file']['to_do']
        self.conll_meta = infos['conll_file']['metadata']
        self.conll_w_gold = infos['conll_file']['with_gold_labels']
        self.txt_todo = infos['txt_file']['to_do']
        self.txt_meta = infos['txt_file']['metadata']


class Process:
    def __init__(self, infos):
        self.main = infos["main"] # train test annotation

        self.pre_process_to_do = infos['pre-processing']['to_do']
        self.synt_tool = infos['pre-processing']['syntactic_tool']
        self.synt_parse = infos['pre-processing']['syntactic_parsing']
        self.toke = infos['pre-processing']['tokenization']
        self.ssplit = infos['pre-processing']['sentence_split']
        self.crea_meta = infos['pre-processing']['create_metadata']['to_do']
        self.meta_line = infos['pre-processing']['create_metadata']['line']
        self.meta_sent = infos['pre-processing']['create_metadata']['sent']
        self.eval = infos['evaluation']

        if self.main == "train" or "fine_tune":
            self.set_train = infos['discourse_segmenter']['training']['train_data_path']
            self.set_dev = infos['discourse_segmenter']['training']['validation_data_path']
            self.set_test = infos['gold_test_data_path']

        self.toolkit = infos['discourse_segmenter']['training']['toolkit']
        self.tr_config = infos['discourse_segmenter']['training']['config_file']
        self.pretr_lm = infos['discourse_segmenter']['training']['pre_trained_lm']

        self.model = infos['discourse_segmenter']['model'] # ezpz for Tony 
        self.test_data = infos['gold_test_data_path']


    def get_model(self):
        self.model_path = ""
        if self.model == "tony": 
            arch = "french_tokens.tar.gz"
            if not os.path.isfile(f"../model/tony/{arch}"):
                dl = "wget https://zenodo.org/record/4235850/files/french_tokens.tar.gz -P ../model/tony --progress=bar"
                os.system(dl)
                self.model_path = f"../model/tony/{arch}"
            else:
                print("----> Tony already in place !")
                self.model_path = f"../model/tony/{arch}"
        else:
            self.model_path = self.model

    def get_data_for_train(self, data):
        # from names get path to input
        self.train_raw = f"{data.path}/{self.set_train}{data.exte}"
        self.dev_raw = f"{data.path}/{self.set_dev}{data.exte}"
        self.test_raw = f"{data.path}/{self.set_test}{data.exte}"

    def get_data_for_fine_tune(self, data):
        """
        spec: testset is the same that data_raw_name / 
              trainset & devset are elsewhere and config fill with path not just name
        """
        self.train_raw = self.set_train
        self.dev_raw = self.set_dev
        self.test_raw = f"{data.path}/{self.set_test}{data.exte}"
        # reset names to go ez pz for ner formatage
        self.set_train = re.sub('\.[^\.]+$', '', re.sub('^.*/', '', self.set_train))
        self.set_dev = re.sub('\.[^\.]+$', '', re.sub('^.*/', '', self.dev_raw))

    def make_sets_ner_format(self, data): #[steps.set_train, steps.set_dev, steps.set_test] 
        self.train_ner = f"{data.conv}/{self.set_train}{data.exte}.ner"
        self.dev_ner = f"{data.conv}/{self.set_dev}{data.exte}.ner" 
        self.test_ner = f"{data.conv}/{self.set_test}{data.exte}.ner" 
        print(f"----> Making NER format {self.train_ner}.")
        conv_to_ner.main(self.train_raw, self.train_ner, "conll")
        print(f"----> Making NER format {self.dev_ner}.")
        conv_to_ner.main(self.dev_raw, self.dev_ner, "conll")
        print(f"----> Making NER format {self.test_ner}.")
        conv_to_ner.main(self.test_raw, self.test_ner, "conll")

    def update_training_config(self):
        logs.add_json('training_config', self.tr_config)
        self.tr_config_updated = re.sub('.jsonnet$', '_up.jsonnet', self.tr_config)
        with open(self.tr_config, 'r') as js:
            tr_conf = json.load(js)
        tr_conf['train_data_path'] = self.train_ner
        tr_conf['validation_data_path'] = self.dev_ner
        with open(self.tr_config_updated, 'w') as js:
            json.dump(tr_conf, js)
        logs.add_json('training_config_updated', self.tr_config_updated)

    def training(self, data):
        cmd = f"allennlp train -s {data.resu_train} {self.tr_config_updated} &> {data.resu}/logs_training_{data.stamp_time}.txt"
        cmd = cmd if data.over == False else re.sub('&>', '-f &>', cmd)
        print(f"----> Training : {cmd}")
        os.system(cmd)
        steps.model_path = f"{data.resu_train}/model.tar.gz"
        logs.add_infos('model_to make predictions', self.model_path)
        logs.add_infos('logs_trainning_file', f"{data.resu}/logs_training_{data.stamp_time}.txt" )

    def fine_tuning(self, data):
        logs.add_infos('model_to be fine-tuned', self.model)
        cmd = f"allennlp fine-tune -m {self.model_path} -c {self.tr_config_updated} -s {data.resu_fine} &> {data.resu}/logs_fine-tuning_{data.stamp_time}.txt"
        print(f"----> Fine-tuning : {cmd}")
        os.system(cmd)
        self.model_ft_path = f"{data.resu_fine}/model.tar.gz"
        logs.add_infos('model_to make predictions', self.model_ft_path)
        logs.add_infos('logs_fine-tuning_file', f"{data.resu}/logs_fine-tuning_{data.stamp_time}.txt")


def get_stamp():
    now = datetime.now()
    stamp = re.sub('[\s:]', '_', str(now))
    return stamp

def get_config_infos(config, stamp, stamp_time, logs, overwrite):
    with open(config, 'r', encoding='utf-8') as f:
        infos = json.load(f)
        data = Data(infos['data_raw'], stamp, stamp_time, overwrite)
        steps = Process(infos['steps'])
        prod = Output(infos['output'])
        logs.add_infos('config', infos)
    return data, steps, prod


class Logs:
    def __init__(self):
        self.dict = {}

    def add_infos(self, key, value):
        self.dict[key] = value

    def add_json(self, key, jsonf):
        with open(jsonf, 'r', encoding='utf-8') as f:
            infos = json.load(f)
            self.dict[key] = infos

    def print(self, stamp_time):
        self.file_path = f"{data.run}/logs_global_{stamp_time}.json"
        with open(self.file_path, 'w', encoding='utf-8') as fl:
            json.dump(self.dict, fl, indent=4)


if __name__ == '__main__':
    stamp = stamp_time = get_stamp()
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', help='Config file in JSON.')
    parser.add_argument('--name',default=stamp , help='Run name.')
    parser.add_argument('-o', '--overwrite', action='store_true', help='Overwite output.')
    args = parser.parse_args()
    config = args.config
    stamp = args.name
    overwrite = args.overwrite
    
    logs = Logs()
    data, steps, prod = get_config_infos(config, stamp, stamp_time, logs, overwrite)
    data.create_folders()
    
    logs.add_infos("stamp", stamp)
    logs.add_infos("stamp_time", stamp_time)
    logs.add_infos("overwrite", overwrite)

    if steps.main == "annotation" or steps.main == "test":
        data.pre_processing(steps)
        data.make_ner_format()
        steps.get_model()
        data.make_predictions(steps) # output allennlp JSON
        if steps.eval == True:
            data.evaluation(steps, prod)
        else:
            data.make_output(prod)
    
    elif steps.main == "train":
        steps.get_data_for_train(data) #[steps.set_train, steps.set_dev, steps.set_test] 
        data.pre_processing(steps, file_in=steps.test_raw)
        steps.make_sets_ner_format(data)
        steps.update_training_config()
        steps.training(data)
        data.make_predictions(steps, js_name=steps.set_test, fi_ner=steps.test_ner)
        if steps.eval == True:
            data.evaluation(steps, prod, name=steps.test_data)
    
    elif steps.main == "fine_tune":
        steps.get_data_for_fine_tune(data)
        data.pre_processing(steps, file_in=steps.test_raw)
        steps.make_sets_ner_format(data)
        steps.get_model() # model to be fine-tune
        steps.update_training_config()
        steps.fine_tuning(data)
        data.make_predictions(steps, js_name=steps.set_test, fi_ner=steps.test_ner, model=steps.model_ft_path)
        if steps.eval == True:
            data.evaluation(steps, prod, name=steps.test_data, model=steps.model_ft_path)

    logs.print(stamp_time)
    print(f"----> All logs saved in {logs.file_path}")