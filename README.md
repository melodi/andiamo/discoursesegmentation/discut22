# Project DisCut22 : Discourse Annotator Tool

A tool for Discourse Annotation. Inheritor of ToNy and DisCut, segmentors for DISRPT 2019 and 2021. The goal of this version is to be easy to use with or without IT knowledge.

__2021__  
*[Multi-lingual Discourse Segmentation and Connective Identification: MELODI at Disrpt2021](https://aclanthology.org/2021.disrpt-1.3.pdf)*  
Code: https://gitlab.irit.fr/melodi/andiamo/discoursesegmentation/discut

__2019__  
*[ToNy: Contextual embeddings for accurate multilingual discourse segmentation of full documents](https://www.aclweb.org/anthology/W19-2715.pdf)*  
Code: https://gitlab.inria.fr/andiamo/tony




## Usecases 
- **Discourse Segmentation: "annotation"** Take a raw text as input, use a loaded model to make predictions. Output the same text but with EDU segmentation.  
&rarr; `config_global_1.1.json` and `config_global_1.2.json`.  
- **Segmentation Evaluation: "test"** Take an EDU gold segmented text as input, use a loaded model to make predictions. Output scores of model predictions against gold, and output discrepancies.  
&rarr; `config_global_2.json`.  
- **Custom Model Creation: "train"** Train a new model using a pretrained Language Model (BERT, etc) and a specific dataset or combination of datasets. Then make predictions and evaluation.  
&rarr; `config_global_3.json`.
- **Custom Model fine-tuning: "fine_tune"** Fine-tune an existing model using a pretrained Language Model (BERT, etc) and a specific dataset or combination of datasets. Then make predictions and evaluation.  
&rarr; `config_global_4.json`.

## Content description  

- `README.md` Description of project.  
- `global_config_file_guideline.md` Contains detailed documentation to build well formed config_global file.
- `data/my_cool_dataset/` Contains raw data that need the same name of the directory.
- `code/` Contains main scripts.  
    - `config_global_XX.json` A file to be completed for your specific project.  
    - `utils/` Contains useful scripts to be called.  
    - `discut22_1.py` One python script to run them all.  
- `model/` Contains model to be loaded.
    - `config_training.jsonnet` A file to be completed for usecases 3 and 4.
- `projects/` This directory will be created automatically.
    - `my_cool_exp_v1/` Name of your run. This directory will be created automatically. (see Usage)
        - `logs_global.json` Logs of all processes, data and results. This file will be created automatically.
        - `data_converted/` Contains pre-processed data if needed. This directory will be created automatically.
        - `results/` Contains output files, logs and metrics, if any. This directory will be created automatically.
            - `train/` Contains specific output related to train (like the model created), if any.
            - `fine_tune/` Contains specific output related to train (like the model created), if any.


## Set up environnement
- DICUT22 run on Python 3.7. Advise : create a specific virtual envireonment (Miniconda...).
- Install all librairies required with the following command:
```
pip install -r requirements.txt
```
- Install pytorch:
```
pip install torch==1.9.0+cu111 torchvision==0.10.0+cu111 torchaudio===0.9.0 -f https://download.pytorch.org/whl/torch_stable.html
```


## Configuration file: to chose or to complete
- `code/config_global_X.json` &rarr; See `global_config_file_guideline.md`.


# Usage
(go to `code` directory)  
Run the command:
```
python discut22_2.py --config config_XX.json [--name my_run_name] [-o]
```
--config <> &nbsp; &nbsp; &nbsp; &nbsp; Your config file. (Mandatory)  
--name <> &nbsp; &nbsp; &nbsp; &nbsp; A name for your run. (Optional)  
-o, --overwrite &nbsp; &nbsp; &nbsp; &nbsp; Allow overwriting of `data_converted/` and `results/`. (optional)


## Support
laura.riviere@irit.fr


## Authors and acknowledgment
Morteza Ezzabady  
Laura Rivière  
Amir Zeldes  



## License
Copyright 2023 IRIT-MELODI
<!---

## Test and Deploy

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
## License
## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

--->